# Validating Lightning Signer

Improving Lightning security with fully validated remote signing.

You can go directly to the [code repository for VLS](https://gitlab.com/lightning-signer/validating-lightning-signer).

You can also go to the [VLS website](https://vls.tech/).

## Motivation

[Lightning nodes are in effect hot
wallets](https://medium.com/@devrandom/securing-lightning-nodes-39410747734b?)
with substantial balances that must stay on-chain to provide channel
liquidity.

## Proposed Solution

We propose to sequester the private keys and secrets in one or more
hardened policy signing devices.  We have a reference
[Validating Lightning Signer implementation](https://gitlab.com/lightning-signer/validating-lightning-signer) in Rust.
It currently has a gRPC interface, but other APIs are possible.

When run in external signing mode the Lightning node would use an alternate
signing module which replaces signing with proxy calls to the policy
signing devices.

The external signing device applies a complete set of policy controls
to ensure that the proposed transaction is safe to sign. Having a
[complete set of policy controls](policy-controls.md)
protects the funds even in the case of
a complete compromise of the node software. This will require some
overlap in logic between the node software and the policy signer.

<div align="center">
    <img src="./overview/system-overview.svg" width="500" height="300">
</div>

We also have a reference node implementation named [lnrod (Lightning Rod)](https://gitlab.com/lightning-signer/lnrod) in progress.


## Diagrams

#### [Protocol Sequence Diagrams](./seq-diagrams/README.md)

#### [Transaction Signing Diagrams](./tx-diagrams/README.md)

#### [Routing Policy Enforcement Walkthrough (PDF)](./routing-diagrams/partial-fail.pdf)

#### [System Details](./overview/README.md)

## Roadmap

The development of this approach has several distinct stages.  You
can see the [project roadmap here](roadmap.md).

## Chat

You can join https://matrix.to/#/#lightning-signer:libera.chat.

## Documents

* [Securing Lightning Nodes](https://medium.com/@devrandom/securing-lightning-nodes-39410747734b?)

* [Lightning Signing Policy Controls](policy-controls.md)


## Remote Lightning Signer Service

This is a reference implementation of the VLS.  The
current implementation is capable of complete external signing with
the [`c-lightning` remote signer
proxy](https://github.com/lightning-signer/c-lightning/pull/6).
Current development is now focused on policy enforcement and support
for additional lightning node implementations.

## Remote Signer API

See the [gRPC definition](https://gitlab.com/lightning-signer/validating-lightning-signer/-/blob/master/src/server/remotesigner.proto).

We also have preliminary support for the C-Lightning `hsmd` wire protocol.

## Node Implementation Progress

These include changes to architecture and interfaces to make the node
software compatible with the Signer Service.

### C-Lightning Progress

Status: Our branch of C-Lightning replaces `hsmd` with a proxy and a reference implementation of the Lightning Signer.  This configuration passes the vast majority of C-Lightning integration tests.

* Ongoing - [WIP: Remote Signer Proxy](https://github.com/lightning-signer/c-lightning/pull/6)
* Redo - [Added hsm_new_channel and hsm_ready_channel messages. #3745](https://github.com/ElementsProject/lightning/pull/3745)
* Merged - [hsmd: Added fields to hsm_sign_remote_commitment_tx to allow complete validation. #3363](https://github.com/ElementsProject/lightning/pull/3363)
* Merged - [lightningd: Added --subdaemon command to allow alternate subdaemons. #3372](https://github.com/ElementsProject/lightning/pull/3372)

### Rust-Lightning Progress

Status: The Rust-Lightning `Sign` trait has been augmented to match the Lightning Signer API, and these changes were merged upstream.  

* Merged - [Draft: Introduce CommitmentTransactionInfo #742](https://github.com/rust-bitcoin/rust-lightning/pull/742)
* Merged - [ChannelKeys - provide to_self_delay alongside the remote channel pubkeys #658](https://github.com/rust-bitcoin/rust-lightning/pull/658)
* Merged - [ChannelKeys - separate commitment revocation from getting the per-commitment point #655](https://github.com/rust-bitcoin/rust-lightning/pull/655)
* Merged - [Introduce OnchainTxHandler, move bumping and tracking logic #462](https://github.com/rust-bitcoin/rust-lightning/pull/462)
* Merged - [Add ChannelKeys to ChannelMonitor #466](https://github.com/rust-bitcoin/rust-lightning/pull/466)

### LND Progress

Status: Incomplete

* Obsolete - [WIP: RemoteSigner Implementation](https://github.com/lightning-signer/lnd/pull/10)
* Obsolete - [WIP: Elevate signing operations to ContextSigner interface](https://github.com/lightning-signer/lnd/pull/9)
* Submitted - [Correct BIP-32 derivation issue #182](https://github.com/btcsuite/btcutil/pull/182)
* Merged - [Create watch-only address managers and accounts #667](https://github.com/btcsuite/btcwallet/pull/667)

### Eclair Progress

Status: Not started

* Obsolete - [Modifications to eclair](https://github.com/lightning-signer/eclair)
