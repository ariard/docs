### [Back to Lightning-Signer Home](../README.md)

These diagrams show the remote signing API calls in the context of the
[BOLT #2](https://github.com/lightningnetwork/lightning-rfc/blob/master/02-peer-protocol.md)
specified protocol.

<br>

Diagrams maintained using [app.diagrams.net](https://app.diagrams.net/)

## Channel Establishment Sequence

[BOLT #2 - Channel Establishment](https://github.com/lightningnetwork/lightning-rfc/blob/master/02-peer-protocol.md#channel-establishment)

<br>

<div align="center">
    <img src="./channel-establishment.svg">
</div>

<br>

## Normal Operation Sequence

[BOLT #2 - Normal Operation](https://github.com/lightningnetwork/lightning-rfc/blob/master/02-peer-protocol.md#normal-operation)

<br>

<div align="center">
    <img src="./normal-operation.svg">
</div>

<br>

